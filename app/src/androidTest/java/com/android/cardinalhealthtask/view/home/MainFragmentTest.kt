package com.android.cardinalhealthtask.view.home

import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.android.cardinalhealthtask.R
import com.android.cardinalhealthtask.view.home.viewmodel.HomeViewModel
import com.google.android.material.snackbar.Snackbar
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainFragmentTest {

    @JvmField
    @Rule
    val mActivityRule = ActivityTestRule<MainActivity>(MainActivity::class.java, false, true)

    private lateinit var mViewModel: HomeViewModel

    @Before
    fun setup() {
        val fragment =
            mActivityRule.activity.supportFragmentManager.findFragmentByTag(
                MainFragment::class.java.simpleName
            ) as MainFragment
        mViewModel = fragment.getViewModel()
    }

    /**
     * Test case - Display edit text and can perform click, enter text and imei action
     * Expected result - Show progress view, enter text, check if keyboard hide and imei action
     */
    @Test
    fun verify_editTestActionEvent() {
        val appCompatEditText = onView(
            allOf(
                withId(R.id.editText),
                isDisplayed()
            )
        )
        appCompatEditText.perform(click())


        val appCompatEditText2 = onView(
            allOf(
                withId(R.id.editText),
                isDisplayed()
            )
        )
        appCompatEditText2.perform(replaceText("android"), closeSoftKeyboard())

        val appCompatEditText3 = onView(
            allOf(
                withId(R.id.editText), withText("android"),
                isDisplayed()
            )
        )
        appCompatEditText3.perform(pressImeActionButton())


    }

    /**
     * Test case - Display progress view
     * Expected result - Show progress view
     */
    @Test
    fun verify_showProgressView() {
        onView(withId(R.id.progress_view)).check(matches(isDisplayed()))
    }

    /**
     * Test case - Display gallery grid
     * Expected result - Show gallery
     */
    @Test
    fun verify_showGalleryData() {
        Thread.sleep(5000)
        onView(ViewMatchers.withId(R.id.recycler_gallery_list))
            .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE))).check(
                matches(
                    ViewMatchers.hasMinimumChildCount(1)
                )
            )
    }

    /**
     * Test case - Show network error snack bar message
     * Expected result - Show network error snack bar
     */
    @Test
    fun verify_showErrorToast() {
        mViewModel.showSnackBarAction("networkError", "Okay", 0, Snackbar.LENGTH_LONG)
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText("networkError")))
    }


    private fun childAtPosition(
        parentMatcher: Matcher<View>,
        position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }
}

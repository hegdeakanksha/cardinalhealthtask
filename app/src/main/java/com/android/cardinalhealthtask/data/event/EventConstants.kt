package com.android.cardinalhealthtask.data.event

class EventConstants {
    companion object {
        const val TOOLBAR_TITLE_EVENT = 100
        const val SORT_BY_TAKEN_DATE = 101
        const val SORT_BY_PUBLISHED_DATE = 102
        const val SORT_BY_NONE = 103
    }
}
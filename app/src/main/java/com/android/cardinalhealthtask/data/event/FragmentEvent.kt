package com.android.cardinalhealthtask.data.event

import android.os.Bundle

class FragmentEvent {
    var fragment: Int? = 0
    var bundle: Bundle? = null
}
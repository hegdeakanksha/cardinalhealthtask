package com.android.cardinalhealthtask.network;

import retrofit2.Retrofit;

/**
 * Api service creation goes here check if its null create new one
 */
public class RestClient {
    static RestService restService = null;


    public static RestService getClient() {
        if (restService == null) {
            Retrofit retrofit = RestApiClient.getApiClient().getRetrofit();
            restService = retrofit.create(RestService.class);
        }
        return restService;
    }

}

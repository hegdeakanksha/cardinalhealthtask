package com.android.cardinalhealthtask.network;


import com.android.cardinalhealthtask.model.GalleryFeedData;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Retrofit is the class through which your RestService API interfaces are turned into callable objects
 */
public interface RestService {
    @GET("photos_public.gne")
    Observable<Response<GalleryFeedData>> getGalleryFeed(@QueryMap Map<String, String> options);
}

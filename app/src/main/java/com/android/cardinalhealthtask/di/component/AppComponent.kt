package com.android.cardinalhealthtask.di.component

import android.app.Application
import com.android.cardinalhealthtask.app.CHApplication
import com.android.cardinalhealthtask.di.builder.MainActivityBuilder
import com.android.cardinalhealthtask.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class, AppModule::class, MainActivityBuilder::class]
)
interface AppComponent : AndroidInjector<CHApplication> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun create(app: Application):Builder

        fun build(): AppComponent
    }
}
package com.android.cardinalhealthtask.di.module

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.android.cardinalhealthtask.di.builder.MainFragmentBuilder
import com.android.cardinalhealthtask.network.RestApiClient
import com.android.cardinalhealthtask.network.RestService
import com.android.cardinalhealthtask.utils.NavigationUtils
import com.android.cardinalhealthtask.utils.rxbus.RxBus
import com.android.cardinalhealthtask.utils.rxbus.RxBusImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [(MainFragmentBuilder::class)])
class AppModule {
    @Provides
    @Singleton
    fun provideRxBus(): RxBus {
        return RxBusImpl()
    }

    @Provides
    @Singleton
    fun provideAppNavigator(): NavigationUtils {
        return NavigationUtils()
    }

    @Provides
    @Singleton
    fun provideRestService(): RestService {
        val retrofit = RestApiClient.getApiClient().retrofit
        return retrofit.create(RestService::class.java)
    }

    @Provides
    @Singleton
    fun provideSharedPreference(app: Application): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(app)
}
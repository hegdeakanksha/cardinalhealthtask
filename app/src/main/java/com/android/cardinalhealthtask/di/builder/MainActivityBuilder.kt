package com.android.cardinalhealthtask.di.builder

import com.android.cardinalhealthtask.view.home.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class MainActivityBuilder {

    @ContributesAndroidInjector(modules = [MainFragmentBuilder::class])
    internal abstract fun mainActivity(): MainActivity
}
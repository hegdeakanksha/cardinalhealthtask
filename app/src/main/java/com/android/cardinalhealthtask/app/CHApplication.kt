package com.android.cardinalhealthtask.app

import android.content.Context
import androidx.multidex.MultiDex
import com.android.cardinalhealthtask.di.component.DaggerAppComponent
import com.android.cardinalhealthtask.network.RestApiClient
import com.android.cardinalhealthtask.utils.CHConstants
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * CHApplication encloses app level data and component initialization
 */
class CHApplication : DaggerApplication() {
    init {
        instance = this
    }

    companion object {
        private var instance: CHApplication? = null

        /**
         * Return WSApplication instance
         */
        fun applicationContext(): Context {
            return instance!!.applicationContext
        }

        /**
         * Returns base url
         */
        fun getBaseUrl(): String {
            return CHConstants.BASE_URL
        }
    }

    override fun onCreate() {
        super.onCreate()
        initSetup()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        initMultidex()
    }

    private fun initSetup() {
        setUpClient()
    }

    /**
     * Performs members-injection
     */
    override fun applicationInjector(): AndroidInjector<out CHApplication> {
        return DaggerAppComponent.builder().create(this).build()
    }

    /**
     * init multidex
     */
    private fun initMultidex() {
        MultiDex.install(this)
    }

    /**
     *Init retrofit client
     */
    private fun setUpClient() {
        RestApiClient.ApiClientBuilder()
            .baseUrl(getBaseUrl())
            .resetClient(false)
            .build()
    }
}
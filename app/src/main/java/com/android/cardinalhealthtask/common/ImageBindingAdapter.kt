package com.android.cardinalhealthtask.common

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.android.cardinalhealthtask.R
import com.squareup.picasso.Picasso

/**
 * An ImageBindingAdapter is responsible for making the appropriate framework calls to set values
 */
object ImageBindingAdapter {
    /**
     * SetImageUrl method load and bind image from url using Picasso library
     */
    @JvmStatic
    @BindingAdapter("app:galleryImage")
    fun setImageUrl(view: ImageView, url: String?) {
        url?.let {
            val aUrl = it!!.replace("\\", "")
            Picasso.with(view.context)
                .load(aUrl)
                .placeholder(R.drawable.icn_image_placeholder)
                .into(view)
        } ?: Picasso.with(view.context)
            .load(R.drawable.icn_image_placeholder)
            .placeholder(R.drawable.icn_image_placeholder)
            .into(view)
    }
}

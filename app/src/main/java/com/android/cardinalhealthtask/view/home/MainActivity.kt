package com.android.cardinalhealthtask.view.home

import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import com.android.cardinalhealthtask.R
import com.android.cardinalhealthtask.data.event.CHEvent
import com.android.cardinalhealthtask.data.event.EventConstants
import com.android.cardinalhealthtask.databinding.ActivityMainBinding
import com.android.cardinalhealthtask.utils.FragmentConstants
import com.android.cardinalhealthtask.utils.CHConstants
import com.android.cardinalhealthtask.view.base.BaseActivity
import kotlinx.android.synthetic.main.toolbar_normal.*

/**
 * MainActivity encloses fragment
 */
class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )
        initToolbar(getToolbar())
        addMainFragment()
    }

    /**
     * Init toolbar
     */
    private fun initToolbar(toolbar: Toolbar?) {
        toolbar?.run {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            invalidateOptionsMenu()
            txt_toolbar_title.text = CHConstants.TITLE_HOME
            icn_sort.setOnClickListener {
                showPopup(this)
            }
        }
    }

    /**
     * Get toolbar
     */
    private fun getToolbar(): Toolbar? {
        return findViewById(R.id.toolbar_normal)
    }

    /**
     * Add fragment to activity
     */
    private fun addMainFragment() {
        addFragment(
            null,
            FragmentConstants.FRAGMENT_MAIN_FRAGMENT,
            FragmentConstants.FRAG_ADD,
            R.id.main_fragment_container,
            false
        )
    }

    private fun showPopup(v: View) {
        PopupMenu(this, v, Gravity.END).apply {
            setOnMenuItemClickListener { item ->
                when (item?.itemId) {

                    R.id.sort_date_taken -> {

                        postMenuEvent(EventConstants.SORT_BY_TAKEN_DATE)
                        true
                    }
                    R.id.sort_published_date -> {

                        postMenuEvent(EventConstants.SORT_BY_PUBLISHED_DATE)

                        true
                    }
                    R.id.sort_none -> {

                        postMenuEvent(EventConstants.SORT_BY_NONE)

                        true
                    }
                    else -> false
                }
            }
            inflate(R.menu.popup_menu)
            show()
        }
    }

    private fun postMenuEvent(menuEvent: Int) {
        var event = CHEvent()
        event.event = menuEvent
        rxBus.send(event)
    }
}

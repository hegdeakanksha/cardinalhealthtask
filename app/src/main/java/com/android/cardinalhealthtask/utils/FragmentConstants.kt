package com.android.cardinalhealthtask.utils

/**
 * FragmentConstants class holds all the constants correspond to fragment transition
 */
class FragmentConstants {
    companion object {
        const val FRAG_ADD = 100
        const val FRAG_REPLACE = 101
        const val FRAG_ADD_ANIMATE = 102
        const val FRAG_DIALOG = 103
        const val FRAG_ADD_WITH_STACK = 104
        const val FRAG_REPLACE_WITH_STACK = 105
        const val FRAGMENT_MAIN_FRAGMENT = 1
    }
}
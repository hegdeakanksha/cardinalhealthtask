package com.android.cardinalhealthtask.utils

import androidx.fragment.app.Fragment
import com.android.cardinalhealthtask.view.home.MainFragment

/**
 * An FragmentUtils class returns fragment based on {@link FragmentConstants} type
 */
class FragmentUtils {
    companion object {
        fun getFragmentTag(type: Int): Fragment? {
            var fm: Fragment? = null
            when (type) {
                1 -> fm = MainFragment()
            }
            return fm
        }
    }
}
package com.android.cardinalhealthtask.utils

import android.app.Activity
import android.content.Context
import android.text.TextUtils
import android.view.inputmethod.InputMethodManager
import com.android.cardinalhealthtask.network.RestConstants
import java.text.SimpleDateFormat
import java.util.Locale
import kotlin.collections.HashMap

/**
 * A UtilsHelper included common utility methods
 */
class UtilsHelper {

    companion object {
        /**
         * Hide keyboard
         */
        fun hideKeyboard(context: Context?) {
            if (context == null) return
            val activity = context as Activity?
            val inputMethodManager = activity!!.getSystemService(
                Activity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            if (inputMethodManager != null && !activity.isFinishing && activity.currentFocus != null)
                inputMethodManager.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken, 0
                )
        }

        /**
         *Format published date
         */
        fun getFormattedPublishedDate(date: String): String {
            val originalFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH)
            val targetFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH)
            val date = originalFormat.parse(date)
            return targetFormat.format(date)
        }

        /**
         * Format date taken
         */
        fun getFormattedDateTaken(date: String): String {
            val originalFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            val targetFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH)
            val date = originalFormat.parse(date)
            return targetFormat.format(date)
        }

        /**
         * Constructs query map for rest api call
         */
        fun getQueryMap(tag: String): HashMap<String, String> {

            var queryMap: HashMap<String, String> = HashMap()
            queryMap["nojsoncallback"] = RestConstants.NOJSONCALLBACK
            queryMap["format"] = RestConstants.FORMAT
            if (!TextUtils.isEmpty(tag)) {
                queryMap["tags"] = tag
            }
            return queryMap
        }
    }
}
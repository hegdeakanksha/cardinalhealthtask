package com.android.cardinalhealthtask.model

import android.annotation.SuppressLint
import com.android.cardinalhealthtask.data.common.BaseTModel
import com.android.cardinalhealthtask.utils.UtilsHelper
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
data class GalleryFeedItem(
    @SerializedName("title") val title: String,
    @SerializedName("link") val link: String,
    @SerializedName("media") val media: GalleryMedia,
    @SerializedName("date_taken") val dateTaken: String,
    @SerializedName("description") val description: String,
    @SerializedName("published") val publishedDate: String,
    @SerializedName("tags") val tags: String

) : BaseTModel() {

    fun getFormattedPublishedDate(): String {
        return UtilsHelper.getFormattedPublishedDate(publishedDate)
    }

    fun getFormattedDateTaken(): String {
        return UtilsHelper.getFormattedDateTaken(dateTaken)
    }
}

package com.android.cardinalhealthtask.model

import android.annotation.SuppressLint
import com.android.cardinalhealthtask.data.common.BaseTModel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
data class GalleryFeedData(
    @SerializedName("title") val title: String,
    @SerializedName("items") val galleryItemList: ArrayList<GalleryFeedItem>
) : BaseTModel()
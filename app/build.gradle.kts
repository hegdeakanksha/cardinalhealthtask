
plugins {
    id(BuildPlugin.androidApp)
    kotlin(BuildPlugin.kotlinAndroid)
    kotlin(BuildPlugin.kotlinAndroidExt)
    kotlin(BuildPlugin.kotlinKapt)
    id(BuildPlugin.dokkaAndroid)
}

android {
    compileSdkVersion(AndroidSDK.targetSdk)
    defaultConfig {
        applicationId = "com.android.cardinalhealthtask"
        minSdkVersion(AndroidSDK.minSdk)
        targetSdkVersion(AndroidSDK.targetSdk)
        versionCode = AppVersioning.generateVersionCode()
        versionName = AppVersioning.generateVersionName()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        register("release") {
            storeFile = rootProject.file(property("STORE_FILE") as String)
            storePassword = project.property("STORE_PASSWORD") as String
            keyAlias = project.property("KEY_ALIAS") as String
            keyPassword = project.property("KEY_PASSWORD") as String
        }
    }

    buildTypes {
        maybeCreate("release").apply {
            buildConfigField("String", "SERVER_BASE_URL", "\"https://api.flickr.com/services/feeds/\"")
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = true
            isDebuggable = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        maybeCreate("qa").apply {
            initWith(getByName("release"))
            buildConfigField("String", "SERVER_BASE_URL", "\"https://api.flickr.com/services/feeds/\"")
            isDebuggable = true
            isMinifyEnabled = false
            isTestCoverageEnabled = true
            isUseProguard = false
            versionNameSuffix = "_QA"
        }
        maybeCreate("dev").apply {
            initWith(getByName("qa"))
            buildConfigField("String", "SERVER_BASE_URL", "\"https://api.flickr.com/services/feeds/\"")
            versionNameSuffix = "_DEV"
        }
        maybeCreate("debug").apply {
            initWith(getByName("dev"))
            versionNameSuffix = "_DEBUG"
        }
    }

    dataBinding.isEnabled = true

    kapt {
        generateStubs = true
    }


    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    packagingOptions {
        exclude("META-INF/build_release.kotlin_module")
        exclude("META-INF/rxkotlin.properties")
        exclude("META-INF/rxjava.properties")
        exclude("META-INF/ASL2.0")
        exclude("META-INF/LICENSE")
        exclude("META-INF/LICENSE.txt")
        exclude("META-INF/NOTICE")
        exclude("META-INF/NOTICE.txt")
    }
}

dependencies {

    //Android Arch
    implementation(AndroidSupportDependencies.support)
    implementation(AndroidSupportDependencies.constraint)
    implementation(AndroidSupportDependencies.material)
    implementation(AndroidSupportDependencies.kotlinJDK)
    implementation(AndroidSupportDependencies.lifeCycler)
    implementation(AndroidSupportDependencies.legacy)
    implementation(AndroidSupportDependencies.coreKtx)
    kapt(AndroidSupportDependencies.lifeCyclerAnno)
    kapt(AndroidSupportDependencies.androidDataBinding)
    implementation(AndroidSupportDependencies.multidex)

    //Rx
    implementation(RxDependencies.rxAndroid)
    implementation(RxDependencies.rxJava)
    implementation(RxDependencies.rxBinding)

    //Network
    implementation(NetworkDependencies.retrofit)
    implementation(NetworkDependencies.retrofit_gson)
    implementation(NetworkDependencies.retrofit_rx)
    implementation(NetworkDependencies.retrofit_mock)
    implementation(NetworkDependencies.okhttp_logging)

    //DI
    implementation(DIDependencies.dagger)
    implementation(DIDependencies.daggerSupport)
    kapt(DIDependencies.daggerCompiler)
    kapt(DIDependencies.daggerProcessor)
    compileOnly(DIDependencies.jsr250)
    implementation(DIDependencies.inject)

    //Picasso
    implementation(OtherDependencies.picasso)

    //Swipe To Refresh
    implementation(OtherDependencies.swipeToRefresh)

    //Circular progress view
    implementation(OtherDependencies.circularProgressView)

    //Test
    testImplementation(TestDependencies.junit)
    androidTestImplementation(TestDependencies.testRule)
    androidTestImplementation(TestDependencies.runner)
    androidTestImplementation(TestDependencies.espresso)
    androidTestImplementation(TestDependencies.espressoContrib)
    testImplementation(TestDependencies.robolectric)
    testImplementation(TestDependencies.mockitoCore) {
        exclude(group = "org.hamcrest")
    }
    testImplementation(TestDependencies.mockitoInline) {
        exclude(group = "org.hamcrest")
    }
    testImplementation(TestDependencies.mockitoJUnit)
    testImplementation(TestDependencies.mockitoKotlin)
}

tasks.named("check") {
    dependsOn(configurations.getByName("ktlint"))
}

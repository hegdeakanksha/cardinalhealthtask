const val kotlin_version = "1.3.41"

object BuildPlugin {

    private object Versions {
        const val dokka_version = "0.9.17"
        const val gradle_version = "3.5.0"
        const val ktlint = "0.29.0"
    }

    //project/build.gradle.kts
    const val gradlePlugin = "com.android.tools.build:gradle:${Versions.gradle_version}"
    const val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
    const val dokkaGradle =
        "org.jetbrains.dokka:dokka-android-gradle-plugin:${Versions.dokka_version}"
    const val ktlint = "com.github.shyiko:ktlint:${Versions.ktlint}"

    //app build.gradle.kts
    const val androidApp = "com.android.application"
    const val kotlinAndroid = "android"
    const val kotlinAndroidExt = "android.extensions"
    const val kotlinKapt = "kapt"
    const val dokkaAndroid = "org.jetbrains.dokka-android"
}

object AndroidSDK {
    const val minSdk = 25
    const val targetSdk = 29
}

object AppVersioning {
    const val versionMajor = 1
    const val versionMinor = 0
    const val versionPatch = 0
    const val isSnapshot = false
    var versionClassifier: String? = null

    fun generateVersionCode(): Int =
        AndroidSDK.minSdk * 10000000 + versionMajor * 10000 + versionMinor * 100 + versionPatch

    fun generateVersionName(): String {
        var versionName = "${versionMajor}.${versionMinor}.${versionPatch}"
        if (versionClassifier == null && isSnapshot) {
            versionClassifier = "SNAPSHOT"
        }

        if (versionClassifier != null) {
            versionName += "-$versionClassifier"
        }
        return versionName
    }
}

object AndroidSupportDependencies {

    private object Versions {
        const val androidX = "1.1.0"
        const val constaintLayout = "2.0.0-beta3"
        const val lifecycleVersion = "2.2.0"
        const val legacy = "1.0.0"
        const val coreKtx = "1.0.2"
        const val data_binding = "3.0.1"
        const val multidex = "1.0.2"
    }

    const val support = "androidx.appcompat:appcompat:${Versions.androidX}"
    const val constraint = "androidx.constraintlayout:constraintlayout:${Versions.constaintLayout}"
    const val material = "com.google.android.material:material:${Versions.androidX}"
    const val kotlinJDK = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    const val lifeCycler = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleVersion}"
    const val lifeCyclerAnno = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycleVersion}"
    const val legacy = "androidx.legacy:legacy-support-v4:${Versions.legacy}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    const val androidDataBinding = "com.android.databinding:compiler:${Versions.data_binding}"
    const val multidex = "com.android.support:multidex:${Versions.multidex}"

}

object RxDependencies {

    private object Versions {
        const val rx_android = "2.0.1"
        const val rx_java = "2.1.0"
        const val rxbinding = "2.0.0"
    }

    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rx_android}"
    const val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rx_java}"
    const val rxBinding = "com.jakewharton.rxbinding2:rxbinding:${Versions.rxbinding}"
}

object NetworkDependencies {

    private object Versions {
        const val retrofit_version = "2.4.0"
        const val retrofit_adapter = "2.3.0"
        const val okhttp_version = "3.10.0"
    }

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit_version}"
    const val retrofit_gson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit_version}"
    const val retrofit_rx = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit_adapter}"
    const val retrofit_mock = "com.squareup.retrofit2:retrofit-mock:${Versions.retrofit_adapter}"
    const val okhttp_logging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp_version}"
}

object DIDependencies {

    private object Versions {
        const val dagger_version = "2.21"
        const val jsr250_version = "1.0"
        const val inject_version = "1"
    }

    const val dagger = "com.google.dagger:dagger-android:${Versions.dagger_version}"
    const val daggerSupport = "com.google.dagger:dagger-android-support:${Versions.dagger_version}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger_version}"
    const val daggerProcessor =
        "com.google.dagger:dagger-android-processor:${Versions.dagger_version}"
    const val jsr250 = "javax.annotation:jsr250-api:${Versions.jsr250_version}"
    const val inject = "javax.inject:javax.inject:${Versions.inject_version}"
}

object OtherDependencies {

    private object Versions {
        const val picasso_version = "2.5.2"
        const val swipeToRefresh_version = "1.0.0"
        const val circularProgressView = "2.5.0"
    }

    //picasso
    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso_version}"

    //Swipe to refresh
    const val swipeToRefresh =
        "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeToRefresh_version}"

    // Circular progress view
    const val circularProgressView =
        "com.github.rahatarmanahmed:circularprogressview:${Versions.circularProgressView}"


}

object TestDependencies {

    private object Versions {
        const val junit = "4.12"
        const val runner = "1.0.2"
        const val espresso = "3.1.0"
        const val robolectric = "3.6.1"
        const val mockitoCore = "2.13.0"
        const val mockitoJUnit = "2.18.0"
        const val mockitoKotlin = "1.5.0"
        const val testRule = "1.0.2"
    }

    const val junit = "junit:junit:${Versions.junit}"
    const val testRule = "com.android.support.test:rules:${Versions.testRule}"
    const val runner = "com.android.support.test:runner:${Versions.runner}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val espressoContrib = "androidx.test.espresso:espresso-contrib:${Versions.espresso}"

    const val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"

    const val mockitoCore = "org.mockito:mockito-core:${Versions.mockitoCore}"
    const val mockitoInline = "org.mockito:mockito-inline:${Versions.mockitoCore}"
    const val mockitoJUnit = "org.mockito:mockito-junit-jupiter:${Versions.mockitoJUnit}"
    const val mockitoKotlin = "com.nhaarman:mockito-kotlin:${Versions.mockitoKotlin}"
}